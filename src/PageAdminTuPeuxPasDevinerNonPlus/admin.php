<?php

if(!isset($_COOKIE["admin"])){
    setcookie(
    "admin",
    "0",
    time() + (10 * 365 * 24 * 60 * 60) //never expire
    );
    die();
} else {
    if($_COOKIE["admin"] !== "1"){
        include(__DIR__ . "/../static/admin.html");
        die();
    }
}

$sent = false;
if(isset($_REQUEST["username"]) && isset($_REQUEST["commentaire"])) {
    include(__DIR__ . "/../config/database.php");
    $query = "INSERT INTO comments(username, comment) VALUES (?, ?);";

    $stmt = $conn->prepare($query);
    $stmt->bind_param('ss', $_REQUEST["username"], $_REQUEST["commentaire"]);
    $stmt->execute();
    $sent = true;
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Admin Page</title>
    <script src="/js/disconnect.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/util.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
</head>
<body>
    <h1>Page d'administration</h1>
    <h2>Bien joué vous êtes arrivés à la fin du jeu ! </h2>
    <button onclick="disconnect()">Se déconnecter</button>
<?php
if($sent) {
    echo "<h2>Votre commentaire a bien été envoyé !";
}
?>
    <h2>Commentaires:</h2>
<?php 
include(__DIR__ . "/../config/database.php");
$q = "SELECT * FROM comments";
$result = $conn->query($q);
while($row = $result->fetch_assoc()){
    echo "<h3>" . $row["username"] . "</h3>";
    echo "<p>" . $row["comment"] . "</p>";
}
?>
    <form method="POST" action="" name="comments">
        <input type="text" name="username" placeholder="Nom d'utilisateur"/>
        <input type="text" name="commentaire" placeholder="Commentaire"/>
        <button type="submit">Envoyer</button>
    </form>
</body>
</html>

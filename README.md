# AtelierSécu
Atelier sur la cybersécurité à destination de lycéens de bac technos

# Run the application:

Nothing easier !
```bash
$ docker-compose up -d
```

TODO: fill maria automatically (quite buggy atm)

Then access the application on localhost:3000

# Intended vulnerabilities

1. Easy password
2. Type juggling on username and password
3. robots.txt exposing admin web page
4a. SQLInjection
4b. IDOR (Indirect Object Reference)
5. Forgotten .git folder allowing to retrieve the whole source code
6. Clear text password in javascript file
7. Cookie forgeing (admin=0 -> admin=1)

# Exercise path:

1. Deviner premier mot de passe (admin:password) / mot de passe dans le code source
-> envoyer une requête avec comme corps `username[]=&password[]=` validera aussi la connexion, car il y a également une vulnérabilité de type type juggling 
2. ...
3. Découverte du /robots.txt -> découverte d'une nouvelle page
4. Refus de connexion, vérification en javascript donc local -> ouverture de la console développeur et inspection de l'onglet "déboggueur"
5. Fin de l'exercice

Pour approfondir:

IDOR / SQLi sur /api/user.php
XSS à implémenter ? même sans vol de cookie possible

# Où sera localisé l'exercice

Le service est contenairisé avec docker; il est donc facile de le déployer n'importe où tant que docker est disponible.
Je peux l'héberger sur un de mes serveurs, avec une url type `iutvannes.woody.sh` ou `iutvannes.englishweek2022.fr` où iutvannes peut être remplacé par n'importe quoi excepté `www` 

-> Accès facile depuis internet directement, les étudiants ne devraient pas être capable de modifier l'environnement des autres.



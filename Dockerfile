FROM php:8.1-apache

RUN docker-php-ext-install mysqli pdo pdo_mysql && docker-php-ext-enable pdo_mysql

COPY src/ /var/www/html

# Intended vulnerability
COPY .git/ /var/www/html/.git/


EXPOSE 80

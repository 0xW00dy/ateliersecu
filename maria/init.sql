GRANT ALL ON *.* TO 'admin_atelier'@'%';
FLUSH PRIVILEGES;

CREATE DATABASE atelier;

USE atelier;

CREATE TABLE users(
  id INT PRIMARY KEY,
  username VARCHAR(255) NOT NULL UNIQUE,
  password VARCHAR(255) NOT NULL
);

INSERT INTO users VALUES (1, "admin", "password");
INSERT INTO users VALUES (2, "toto", "toto");
INSERT INTO users VALUES (3, "alice", "ilovebob");
INSERT INTO users VALUES (4, "bob", "ilovealice");

CREATE TABLE flag(
  flag VARCHAR(255) NOT NULL 
);

INSERT INTO flag VALUES ('IUT{Br4V0P0uRlInj3ct10n!}');

CREATE TABLE comments(
  id INT PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR(255) NOT NULL,
  comment VARCHAR(1024) NOT NULL
)

